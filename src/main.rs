//    Lords of Time Server, a lobby server for the virtual card game Lords of Time
//    Copyright (C) 2017  Waylon Cude <waylon531@firechicken.net>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#[macro_use] extern crate serde_derive;
extern crate serde;
extern crate rmp_serde;

#[macro_use] extern crate diesel_codegen;
#[macro_use] extern crate diesel;
extern crate dotenv;

extern crate sha3;

extern crate rustls;
extern crate webpki_roots;

use rmp_serde::decode;

use std::io::Write;
use std::net::{TcpListener, TcpStream};
use std::sync::{Arc,Mutex};
use std::thread;
use std::time;

mod auth;
mod challenge;
mod game;
mod ring;
mod verbs;
use game::manager::GameManager;
use ring::{Ring,RingContainer};
use verbs::{ClientVerb,ServerVerb};
fn main() {
    let mut config = rustls::ServerConfig::new();

    //wait for connections
    let listener = TcpListener::bind("0.0.0.0:44937").unwrap();

    //messages is a ring buffer
    //too any messages = overwriting old ones
    
    let messages = Ring::new();
    let message_container = Arc::new(Mutex::new(messages));

    let challenges = Arc::new(challenge::Challenges::new());

    //TODO: Seed gamemanager with starting value
    //to prevent multiple games from having same number
    let manager = Arc::new(Mutex::new(GameManager::new(0)));

    // accept connections and process them, spawning a new thread for each one
    //TODO: wrap stream in tls
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                println!("New client connection");
                let stream_clone = stream.try_clone().unwrap();
                let message_container_clone1 = message_container.clone();
                let username = "Anon".to_string();
                let user = Arc::new(Mutex::new(username));
                let user_clone = user.clone();
                thread::spawn(move || message_poller(stream_clone,RingContainer::new(message_container_clone1),user_clone));
                let message_container_clone2 = message_container.clone();
                let challenges_clone = challenges.clone();
                let manager_clone = manager.clone();
                thread::spawn(move || handle_client(stream,RingContainer::new(message_container_clone2),challenges_clone,user,manager_clone));
            }
            Err(e) => { 
                println!("Client failed to connect: {}",e);
            }
        }
    }
    //spawn main thread
    //processes verbs
}
fn handle_client(mut stream: TcpStream,mut message_container: RingContainer, challenges: Arc<challenge::Challenges>, username: Arc<Mutex<String>>, manager: Arc<Mutex<GameManager>>) {
    let mut de = decode::Deserializer::new(stream.try_clone().unwrap());
    loop {
        match serde::Deserialize::deserialize(&mut de) {
            Ok(verb) => {
                match verb {
                    ClientVerb::Broadcast(message) => {
                        message_container.push_message(message,username.lock().unwrap().clone());
                    },
                    ClientVerb::Auth(user,pass) => {
                        match auth::auth_user(&user,pass) {
                            Ok(()) => { 
                                stream.write(rmp_serde::to_vec(&ServerVerb::Result(Ok(())
                                )).unwrap().as_slice()).expect("Failed to write result to client"); 
                                *username.lock().unwrap() = user;
                            },
                            Err(e) => {
                                stream.write(rmp_serde::to_vec(&ServerVerb::Result(Err(verbs::Error::AuthError(e))
                                )).unwrap().as_slice()).expect("Failed to write result to client"); 
                            }
                        }
                    },
                    ClientVerb::Register(user,pass) => {
                        match auth::add_user(user,pass) {
                            Ok(()) => { 
                                stream.write(rmp_serde::to_vec(&ServerVerb::Result(Ok(())
                                )).unwrap().as_slice()).expect("Failed to write result to client"); 
                            },
                            Err(e) => {
                                stream.write(rmp_serde::to_vec(&ServerVerb::Result(Err(verbs::Error::AuthError(e))
                                )).unwrap().as_slice()).expect("Failed to write result to client"); 
                            }
                        }
                    },
                    ClientVerb::Challenge(user) => {
                        //Send challenge to challenge thread
                        //OR add challenge into list of challenges
                        //If corresponding challenge is in list
                        match challenges.challenge(&*username.lock().unwrap(),&user) {
                            //TODO OH SHIT I FORGOT TO WRITE CHALLENGE TO OPPONENT
                            //Challenge sent
                            false => {
                                stream.write(rmp_serde::to_vec(&ServerVerb::Result(Ok(())
                                )).unwrap().as_slice()).expect("Failed to write reult to client"); 
                                message_container.push(ServerVerb::Challenge(user,username.lock().unwrap().clone()));
                            },
                            //then start game
                            //Start game = thread spun up for game
                            //users communicate with channels?
                            //or maybe mutex + stack
                            true => {
                                let game_num = manager.lock().unwrap().start_game();
                                stream.write(rmp_serde::to_vec(&ServerVerb::GameStarted(username.lock().unwrap().clone(),user,game_num
                                )).unwrap().as_slice()).expect("Failed to write deresult to client"); 
                            }
                        }
                    },
                    _ => {}
                }
            },
            Err(_) => { stream.write(rmp_serde::to_vec(&ServerVerb::Result(
                            Err(verbs::Error::DeserializationError)
                            )).unwrap().as_slice()).expect("Failed to write deserialization error to client"); }
        }
    }
    
}
fn message_poller(mut stream: TcpStream,mut message_container: RingContainer,username: Arc<Mutex<String>>) {
    //TODO: poll for game messages
    loop {
        match message_container.get_messages() {
            Some(m) => {
                //m is a vec of ServerVerb
                for message in m {
                    match message {
                        ServerVerb::Challenge(ref user, _) => {
                            if *user == *(username.lock().unwrap()) {
                                break;
                            }
                        }
                        _ => {}
                    }
                    let bytes = rmp_serde::to_vec(&message).unwrap();
                    //unwrapping is probably okay here
                    //I'm fairly sure mutex lock isn't active
                    stream.write_all(bytes.as_slice()).expect("Message failed to write to client");
                }
            },
            None => {}

        }
        thread::sleep(time::Duration::from_millis(20));
    }
}
