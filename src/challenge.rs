use std::collections::HashMap;
use std::sync::Mutex;
use std::time;

//Challenges are valid for five minutes

pub enum ChallengeError {
    AlreadyExists

}
//type Challenge = (String,String,time::Instant) //Username challenged Username
//Maps all challenges to user that is challenged
pub struct Challenges {
    challenge_list: Mutex<HashMap<String,Vec<(String,time::Instant)>>>
}
impl Challenges {
    pub fn new() -> Challenges {
        Challenges{ challenge_list: Mutex::new(HashMap::new()) }
    }
    //True is challenge is completed and a game is about to start
    //False if previous challenge was updated
    pub fn challenge(&self,challenger: &String, challenged: &String) -> bool {
        let challenge_time = time::Duration::from_secs(5*60);
        let time = time::Instant::now();
        //First, check if the challenged has challenged the challenger
        //in the recent past
        let mut challenges = self.challenge_list.lock().unwrap();
        let mut new_list = Vec::new();
        match challenges.get_mut(challenger) {
            //Got through list of challenges
            Some(list) => { //list is &mut Vec<>
                if list.iter().filter(|x| (x.0 == *challenged) && (x.1.elapsed() <= challenge_time)).collect::<Vec<&(String,time::Instant)>>().len() > 0 {
                    new_list = list.drain(1..).filter(
                        |x| x.0 != *challenged 
                        ).collect::<Vec<(String,time::Instant)>>();
                    //If new list is empty then remove it's entry from the hashmap
                    if new_list.len() != 0 {
                        *list = new_list;
                    }
                    return true;
                }
            },
            //Move on to next step immediately
            None => {},
        }
        //MINOR ISSUE: called even when there is no key
        if new_list.len() == 0 {
            challenges.remove(challenger);
        }
        //otherwise add challenger into challenged's list of challenges
        match challenges.get_mut(challenged) {
            //TODO Remove old entries
            Some(list) => {
                list.push((challenger.clone(),time));
                return false;
            },
            //Create new entry in hashmap
            //Pass cause challenges is in scope
            None => {}
        }
        challenges.insert(challenged.clone(),vec!((challenger.clone(),time)));
        false
    }
}
