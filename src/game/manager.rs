//    Lords of Time Server, a lobby server for the virtual card game Lords of Time
//    Copyright (C) 2017  Waylon Cude <waylon531@firechicken.net>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::sync::{Arc,Mutex};
use std::sync::mpsc::{Receiver,Sender,channel};
use std::thread;

use game::GameState;
use verbs;

type Username = String;
//Mutex or channel?

//Maybe store state of finished games in sql table
//I'll need to load some basic state from sql
//Minimum is counter so games have unique ids
pub struct GameManager {
    //Game Id 
    counter: u64,
    //Client passes verbs to game runner
    //and polls for verbs to send to clients
    //and/or directly processes them
    //
    //GetState isn't an action though
    games: HashMap<u64,
        (Sender<(Username,verbs::ClientVerb)>,
        Receiver<(Username,verbs::ServerVerb)>)>
}
impl GameManager {
    pub fn new(counter: u64) -> GameManager {
        GameManager {
            counter:counter,
            games: HashMap::new()
        }
    }
    pub fn start_game(&mut self) -> u64 {
        let prev_counter = self.counter;
        self.counter+=1;
        let game_to_manager = channel();
        let manager_to_game = channel();
        self.games.insert(prev_counter,(manager_to_game.0,game_to_manager.1));
        let mut runner = GameRunner::new(manager_to_game.1,game_to_manager.0);
        thread::spawn(move || runner.run());
        prev_counter
    }
}
//Each gamerunner is on its own thread
//It's safe for the gamerunner to block until user issues input
//It would be safe if there was an infinite turn length
//WAIT THERE'S A RECV TIMEOUT METHOD PRAISE MOZILLA
pub struct GameRunner {
    game: GameState,
    //Indexed by username
    //What happens if a user tries to make multiple connections?
    //Number of Senders should be capped
    listeners: HashMap<String,Vec<Sender<(Username,verbs::Action)>>>,
    //How do I keep track of the Senders?
    incoming: Receiver<(Username,verbs::ClientVerb)>,
    master: Sender<(Username,verbs::ServerVerb)>
}
impl GameRunner {
    pub fn new(recv: Receiver<(Username,verbs::ClientVerb)>,send: Sender<(Username,verbs::ServerVerb)>) -> GameRunner {
        GameRunner {
            game: GameState::new(),
            listeners: HashMap::new(),
            incoming: recv,
            master: send
        }
    }
    pub fn run(&mut self) {
        unimplemented!();


    }
}
