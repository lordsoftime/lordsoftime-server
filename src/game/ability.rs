#[derive(Clone,Serialize,Debug,PartialEq)]
pub enum Ability {
    Flying,
    Breakthrough,
    Timesteal,
    Activate(Effect) //Activatable effect
}
#[derive(Clone,Serialize,Debug,PartialEq)]
pub enum Effect {
    Damage(i32)
}
