//    Lords of Time Server, a lobby server for the virtual card game Lords of Time
//    Copyright (C) 2017  Waylon Cude <waylon531@firechicken.net>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
use game::ability;
use game::{GameState,PlayerWhich};
use game::monster;
use std::sync::Arc;
#[derive(Serialize,Clone,Debug,PartialEq)]
pub struct Card {
    //Effects when card is played
    play: Vec<ability::Effect>,
    //I need some way of having stuff listen for events
    //monsters listen to events
    //and have a bunch of abilities
    //for each event all monsters are checked for matching abilities
}
//Instructions to get to player in World
#[derive(Serialize,Clone)]
pub struct CardId {
    player: PlayerWhich,
    board_index: usize
}
