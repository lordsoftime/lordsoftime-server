//    Lords of Time Server, a lobby server for the virtual card game Lords of Time
//    Copyright (C) 2017  Waylon Cude <waylon531@firechicken.net>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use game::ability;
use game::card::{Card,CardId};
use game::GameState;
#[derive(Clone,Serialize,Debug,PartialEq)]
pub struct Monster {
    health: i64,
    damage: i64,
    //Abilities
    //Check this list each time an event happens
    //Or make a list of listeners for each player in the world
    abilities: Vec<ability::Ability>,
    //This card gets put into graveyard when monster dies
    card: Card,
}
