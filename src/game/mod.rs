//    Lords of Time Server, a lobby server for the virtual card game Lords of Time
//    Copyright (C) 2017  Waylon Cude <waylon531@firechicken.net>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

mod ability;
mod card;
mod monster;
pub mod manager;

use self::card::Card;
use self::monster::Monster;
//Home and Away players
//Home always defends and goes second but gets extra card
//Away always attacks and goes first
#[derive(Clone)]
pub struct GameState {
    away: Player, //Players' hand, field, and discard
    home: Player,

}
impl GameState {
    pub fn send(&self) -> SendState {
        SendState{
            away: self.away.away_send(),
            home: self.home.home_send(),
        }
    }
    pub fn new() -> GameState {
        unimplemented!();
    }
}
#[derive(Serialize,Clone,Debug,PartialEq)]
pub struct SendState {
    away: AwaySendPlayer, //Players' hand, field, and discard
    home: HomeSendPlayer,
}
#[derive(Serialize,Clone,Debug,PartialEq)]
struct AwaySendPlayer {
    health: i64,
    hand: u8,
    board: Vec<Monster>,
    discard: Vec<Card>,
}
#[derive(Serialize,Clone,Debug,PartialEq)]
struct HomeSendPlayer {
    health: i64,
    hand: Vec<Card>,
    board: Vec<Monster>,
    discard: Vec<Card>,
}
#[derive(Clone)]
struct Player {
    health: i64,
    hand: Vec<Card>,
    board: Vec<Monster>,
    discard: Vec<Card>,
    deck: Vec<Card>
}
impl Player {
    pub fn away_send(&self) -> AwaySendPlayer {
        AwaySendPlayer {
            health: self.health,
            hand: self.hand.len() as u8,
            board: self.board.clone(),
            discard: self.discard.clone()
        }
    }
    pub fn home_send(&self) -> HomeSendPlayer {
        HomeSendPlayer {
            health: self.health,
            hand: self.hand.clone(),
            board: self.board.clone(),
            discard: self.discard.clone()
        }
    }
    pub fn new(deck: Vec<Card>) -> Player {
        let mut player = Player {
            health: 90,
            hand: Vec::new(),
            board: Vec::new(),
            discard: Vec::new(),
            deck: deck
        };
        //players start with 7 cards
        for _ in 0 .. 7 {
            player.draw_card().expect("Player should always have enough cards at start of game");
        }
        player
    }
    pub fn get_hand(&self) -> &Vec<Card> {
        &self.hand
    }
    pub fn draw_card(&mut self) -> Result<(),Error> { //TODO: This can error out
        match self.deck.pop() {
            Some(card) => {
                self.hand.push(card); 
                Ok(())
            },
            None => Err(Error::OutOfCards)
        }
    }
}
#[derive(Clone,Serialize)]
pub enum PlayerWhich {
    Home,
    Away
}
#[derive(Debug)]
enum Error {
    OutOfCards,
}
