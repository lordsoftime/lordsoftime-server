//    Lords of Time Server, a lobby server for the virtual card game Lords of Time
//    Copyright (C) 2017  Waylon Cude <waylon531@firechicken.net>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use game;
use auth;

//Maybe bad: Index is a trait in stdlib
type Index = u32;
type Username = String;
type Password = String;
//GameIds probably should be unique
//Some bot can poll finished games for game states
//and record the result somewhere
//#[derive(Serialize,Deserialize,Debug,Clone,PartialEq)]
pub type GameId = u64;

//All u32s will be converted to usize for indexing
#[derive(Deserialize,Serialize,Debug,Clone,PartialEq)]
pub struct Targets {
    self_targets: Vec<Index>,
    opposing_targets: Vec<Index>
}
#[derive(Deserialize,Serialize,Debug,Clone,PartialEq)]
pub enum Action {
    PlayCard(Index,Targets), //Index into hand

    ActivateCard(Index,Index,Targets),  //Activate ability on monster
                                        //Index to monster, index to ability, target

    Attack(Vec<Index>), //Attack with selected monsters

    PassPriority, //Is it called priority?
    EndTurn, //Ends turn, no further actions can be taken
    Surrender, //Immediately ends game
}
#[derive(Serialize,Clone,Debug,PartialEq)]
pub enum Error {
    PermissionDenied,
    AuthError(auth::AuthError),
    DeserializationError,
}
#[derive(Serialize,Deserialize,Debug)]
pub enum ClientVerb {
    Auth(Username,Password),    //User,Pass

    Broadcast(String),      //Broadcast message to all users

    Register(Username,Password),//User, Pass 
                            //Should email be required for registration?
                            //It's probably a good idea for password reset

    Challenge(Username),      //Challenge player with given username
                            //If other player challenges you a game is created
                            //and each player is notified

    Listen(GameId),         //Returns game state
                            //and sends subsequent updates to client

    GetState(GameId),       //Returns game state but not subsequent updates

    DoAction(GameId,Action),//Do an action in the specified game
}
#[derive(Serialize,Clone,Debug,PartialEq)]
pub enum ServerVerb {
    Result(Result<(),Error>),       //Returned on every command not explicitly

    Broadcast(String,Username),              //Sent to all connected players 
                                    // on broadcast

    Challenge(Username,Username),   //User got a challenge from User 2

    GameStarted(Username,Username,GameId), //Sent to all users 
                                    //when game starts
                                    //User1,User2,Game
                                    
    Action(GameId,Action),          //Sent to all listening players
                                    //in a game

    State(Result<(GameId,game::SendState),Error>)   //Returned on GetState and Listen
}
